from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ExportMixin

from call_crm.models import Call, Purpose


@admin.register(Call)
class CallAdmin(ImportExportModelAdmin):
    readonly_fields = ("representative",)
    list_filter = (
        "representative",
        "phone_type",
        "android_version",
        "ios_version",
        "call_time",
    )
    search_fields = ("id", "representative__username", "phone_type")
    list_display = ("id", "representative", "phone_type", "call_time", "call_purpose")

    def save_model(self, request, obj, form, change):
        obj.representative = request.user
        obj.save()


@admin.register(Purpose)
class PurposeAdmin(admin.ModelAdmin):
    search_fields = ("type",)


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email")


class UserAdmin(ExportMixin, UserAdmin):
    resource_class = UserResource
    pass


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
