from django.utils.timezone import now

from django.db import models
from django.contrib.auth.models import User

from call_crm.utils import IOS_VERSIONS, PHONE_TYPE, ANDROID_VERSION


class Purpose(models.Model):
    type = models.TextField(default="General questions")

    def __str__(self):
        return f"{self.type}"


class Call(models.Model):
    customer_id = models.CharField(
        max_length=7,
        null=True,
        blank=True,
        help_text="Optional (if already a customer)",
    )
    representative = models.ForeignKey(User, on_delete=models.CASCADE)
    call_purpose = models.ForeignKey(
        Purpose,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text="Choose from existing or create a new (click on plus)",
    )
    phone_type = models.CharField(
        max_length=30,
        choices=PHONE_TYPE,
        null=False,
        blank=True,
        help_text="Android or iPhone",
    )
    ios_version = models.CharField(
        max_length=10, choices=IOS_VERSIONS, null=True, blank=True
    )
    android_version = models.CharField(
        max_length=20, choices=ANDROID_VERSION, null=True, blank=True
    )
    additional_information = models.TextField(null=True, blank=True)
    call_time = models.DateTimeField(default=now, blank=True, help_text="Default - now")
    call_duration = models.IntegerField(
        null=False, help_text="Required field, min."
    )

    class Meta(object):
        app_label = "call_crm"

    def __str__(self):
        return f"{self.representative}: {self.call_purpose}. Phone: {self.phone_type}"
