#!/usr/bin/env bash
source ~/.bashrc


CONTAINER=mailhog

wait_for_docker(){
    until [ "$(docker inspect -f {{.State.Running}} $CONTAINER)"=="true" ]; do
        echo "Container is not running yet"
        sleep 0.1
    done
    echo "Container running"

}

wait_for_docker
python manage.py migrate
python manage.py collectstatic --noinput

gunicorn brainstack_call_crm.wsgi:application -b 0.0.0.0:8000 --timeout 300 --workers=3 --reload