## Purpose

Project has been created in order to monitor calls in support team

## How to start with docker compose

1. install docker compose: `https://docs.docker.com/compose/install/`
2. build an image project with: `docker-compose build --no-cache`
3. Run docker build `docker-compose up`
4. open the browser with url: `localhost/` or `http://127.0.0.1/`

### Default superuser credentials
Username: `admin`
Password: `admin`

## Additional information

Once you login to the admin page you can create new users ( representatives). You can setu usernames, passwords and personal information.
##### Don't forget to enable checkbox `Staff status`, otherwise representative won't be able to login
The main purpose of it is to grant permissions for users with specific access ( read only or add calls only) `User permissions:`
Once you add required permissions, you can save the user and provide him (her) with a new creadentials.

