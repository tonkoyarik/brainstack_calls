#!/usr/bin/env bash

if virtualenv -p python3.6 venv;then
    echo "VRS_ENV has been created"

else
    read -p "virtualenv is not installed, would you like to install it? (Y/N) " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]];then
        sudo apt-get install python-virtualenv
        virtualenv -p python3.6 venv
        echo "VRS_ENV has been created"
    else
        echo "Bye"
    fi
fi
# Activate venv
. venv/bin/activate
# Install requirementsR

pip install -r requirements.txt

python manage.py migrate

python manage.py runserver